Rails.application.routes.draw do
  mount Rswag::Ui::Engine => '/api-docs'
  mount Rswag::Api::Engine => '/api-docs'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
    scope module: 'api' do
      namespace :v1, defaults: {format: :json} do
        resources :users do 
          collection do 
            post 'create_friend'
            get 'friends'
            post 'common_friends'
            post 'subscribe'
            post 'unsubscribe'
            post 'push_message'
          end
        end
      end
    end
end
