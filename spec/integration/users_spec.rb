require 'swagger_helper' 

describe 'Users API' do

  # All User
  path '/v1/users' do

    get 'Retrieve all users' do
      tags 'Users'
      produces 'application/json'

      response '200', 'All Users' do
        
        before do
          User.create({ firstname: 'dani', lastname: 'himawan', email: 'danihimawan@gmail.com' }) 
          get '/v1/users'
        end
        
        it 'returns a valid 200 response' do 
          expect(response).to have_http_status(200) 
        end
      end

    end
  end

  # Create User
  path '/v1/users' do 
    post 'Create a users' do
    tags 'Users'
    consumes 'application/json'
    parameter name: :user, in: :body, schema: {
      type: :object,
      properties: {
        firstname: { type: :string },
        lastname: { type: :string },
        email: { type: :string }
        }, required: [ 'email' ]
      }

      response '201', 'User created' do
        let(:user) { { firstname: 'dani', lastname: 'himawan', email: 'danihimawan@gmail.com' } }
        context 'returns a valid 201 response' do
          run_test! do |response|
            expect(response.status).to eq(201)
            data = JSON.parse(response.body)
            expect(data["email"]).to eq('danihimawan@gmail.com')
          end
        end
      end

      response "422", 'Invalid Request' do
        let(:user) { { firstname: 'foo' } }
        context 'returns a invalid 422 response' do
          run_test! do |response|
            expect(response.status).to eq(422)
          end
        end

        context 'error cannot blank' do
          run_test! do |response|
            expect(response.status).to eq(422)
            data = JSON.parse(response.body)
            expect(data["email"].last).to eq("can't be blank")
          end
        end 

        # context 'cannot duplicate email address' do
        #   let(:user) { { firstname: 'dani', lastname: 'himawan', email: 'danihimawan@gmail.com' } }
        #   User.create({ firstname: 'dani', lastname: 'himawan', email: 'danihimawan@gmail.com' })
        #   run_test! do |response|
        #     expect(response.status).to eq(422)
        #     data = JSON.parse(response.body)
        #     expect(data["email"].last).to eq("has already been taken")
        #   end
        # end

      end


    end
  end
  
  # Show User
  path '/v1/users/{id}' do

    get 'Retrieve a user' do
      tags 'Users'
      produces 'application/json'
      parameter name: :id, :in => :path, :type => :string

      response '200', 'user found' do
        schema type: :object,
          properties: {
            id: { type: :integer }
          },
          required: [ 'id' ]

        let(:id) { User.create({ firstname: 'dani', lastname: 'himawan', email: 'danihimawan@gmail.com' }).id }
        run_test! do |response|
          expect(response.status).to eq(200)
          data = JSON.parse(response.body)
          expect(data["email"]).to eq("danihimawan@gmail.com")
        end

      end

      response '404', 'user not found' do
        let(:id) { 99 }
        run_test! do |response|
          expect(response.status).to eq(404)
        end
      end

    end
  end

  # Update User
  path '/v1/users/{id}' do

    put 'updated a user' do
      tags 'Users'
      produces 'application/json'
      parameter name: :id, :in => :path, :type => :string
      parameter name: :user, in: :body , schema:{
        type: :object,
          properties: {
            id: { type: :integer },
            firstname: { type: :string },
            lastname: { type: :string },
            email: { type: :string }
          },
          required: [ 'id', 'email' ]
      } 

      let(:available) { User.create({ firstname: 'dup', lastname: 'lastname', email: 'duplicate@gmail.com' }) }
      let(:available2) { User.create({ firstname: 'dup', lastname: 'lastname', email: 'danihimawan@gmail.com' }) }


      response '200', 'user updated' do
        
        let(:id) { User.create({ firstname: 'dani', lastname: 'himawan', email: 'danihimawan@gmail.com' }).id }
        let(:user) { {user: { id: 1, firstname: 'dani', lastname: 'himawan updated', email: 'danihimawan@gmail.com' }} }

        before { put "/v1/users/#{id}", params: user }
        
        it 'returns a valid 200 response' do 
          expect(response).to have_http_status(200) 
          data = JSON.parse(response.body)
          expect(data["lastname"]).to eq("himawan updated")
        end

      end

      response '404', 'user not found' do
        let(:id) { 99 }
        let(:user) { {user: { id: 1, firstname: 'dani', lastname: 'himawan updated', email: 'danihimawan@gmail.com' }} }

        before { put "/v1/users/#{id}", params: user }

        it 'returns a invalid 404 response' do 
          expect(response).to have_http_status(404) 
        end
        

      end

      response '422', 'email already taken ' do
        let(:id) { available2.id }
        let(:user) { {user: { firstname: 'dani', lastname: 'himawan updated', email: available.email }} }
        before do 
          put "/v1/users/#{id}", params: user 
        end

        it 'returns a invalid 422 response' do 
          expect(response).to have_http_status(422) 
        end
        

      end

    end
  end

   # Create Friend
  path '/v1/users/create_friend' do 
    post 'Added new Friend' do
    tags 'Users'
    consumes 'application/json'
    parameter name: :friends, in: :body, schema: {
      type: :object,
      # items: { 
      properties: {
        friends: {
          type: :array,
          items: {
            ref: "#/definitions/address"
          }
        }
      }
    }

    let(:user1) { User.create({ firstname: 'dup', lastname: 'lastname', email: 'duplicate@gmail.com' }) }
    let(:user2) { User.create({ firstname: 'dani', lastname: 'himawan', email: 'danihimawan@gmail.com' }) }
    let(:user3) { User.create({ firstname: 'dani', lastname: 'taufik', email: 'taufik@gmail.com' }) }

    response '201', 'User created' do
      let(:friends) { {friends: ["danihimawan@gmail.com" , "taufik@gmail.com" ]} }
      before do 
        post "/v1/users/create_friend", params: friends
      end

      it "valid 201 response" do 
        expect(response).to have_http_status(200) 
        data = JSON.parse(response.body)
        expect(data["success"]).to eq(true)
      end

    end

      # response '201', 'user updated' do
        
      #   let(:id) { User.create({ firstname: 'dani', lastname: 'himawan', email: 'danihimawan@gmail.com' }).id }
      #   let(:user) { {user: { id: 1, firstname: 'dani', lastname: 'himawan updated', email: 'danihimawan@gmail.com' }} }

      #   before { put "/v1/users/#{id}", params: user }
        
      #   it 'returns a valid 200 response' do 
      #     expect(response).to have_http_status(200) 
      #     data = JSON.parse(response.body)
      #     expect(data["lastname"]).to eq("himawan updated")
      #   end

      # end

      # response "422", 'Invalid Request' do
      #   let(:user) { { firstname: 'foo' } }
      #   context 'returns a invalid 422 response' do
      #     run_test! do |response|
      #       expect(response.status).to eq(422)
      #     end
      #   end

      #   context 'error cannot blank' do
      #     run_test! do |response|
      #       expect(response.status).to eq(422)
      #       data = JSON.parse(response.body)
      #       expect(data["email"].last).to eq("can't be blank")
      #     end
      #   end 
      # end


    end
  end



  end