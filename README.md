# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version
  - ruby-2.4.1 /n 

* System dependencies
  Postgresql

* Configuration
  - configuration file config/application.yml for setting database config

* Database creation

* Database initialization

* How to run the test suite
  - $> rspect

* Services (job queues, cache servers, search engines, etc.)
  - $> rails s 
  - list all documentation api (http://localhost:3000/api-docs/index.html)

* Deployment instructions

* ...
