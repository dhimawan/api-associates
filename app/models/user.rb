class User < ApplicationRecord
  validates :email, uniqueness: true
  validates :email, presence: true

  has_and_belongs_to_many :friends,
      class_name: "User", 
      join_table:  :friends, 
      foreign_key: :user_id, 
      association_foreign_key: :friend_id


    # has_and_belongs_to_many :subscribes,
    #   class_name: "User", 
    #   join_table:  :subscribes, 
    #   foreign_key: :user_id, 
    #   association_foreign_key: :subscribe_id

    has_many :active_subscribers, class_name:  "Subscribe",
                                  foreign_key: "follower_id",
                                  dependent:   :destroy

    has_many :following, through: :active_subscribers,  source: :followed


  def self.create_friends(friend_params)
    status , message = false,"friends email cannot be empty!!"

    unless friend_params.blank?
      status, message, users = check_friend_array(friend_params)
      if status 
        temp_friends = []
        users.each_with_index do |user,index|
          temp_friends = users.to_a
          temp_friends.slice!(index)
          user.friends << temp_friends
          user.save
        end
      end
    end
    return status, message
  end


  def self.check_friend_array(params)
    status , message = false , ""
    availabel_users = User.where("email IN (?) ", params )
    email_available = availabel_users.pluck(:email)
    comparation_email = params - email_available
    if comparation_email.count == 0
      status , message = validate_user_friends(availabel_users)
    else
      status = false
      message = "unregister email #{comparation_email.join(',')}"
    end 

    return status, message, availabel_users
  end

  def self.validate_user_friends(obj_users)
    status , message = true , ""

    temp_friends, active_friends = [], []
    obj_users.each_with_index do |user, index|
      temp_friends = obj_users.to_a
      temp_friends.slice!(index)
      ids = temp_friends.map(&:id)
      available_friends = user.friends.where("friend_id IN (?) ", ids)
      if available_friends.count > 0
        active_friends << available_friends.pluck(:email)
      end
    end
    status , message = false, "User (#{active_friends.join(',')}) already connecting!!" if active_friends.count > 0 
    return status, message
  end


  def self.common_friends(params)
    users = []
    User.where("users.email in (?)",params).each do |user|
      users << user.friends.pluck(:email)
    end
    return users.flatten.uniq
  end

  def self.add_subsribe(requestor, target)
    status, message = false, ""
    followed = User.find_by_email(target)
    follower =  User.find_by_email(requestor)
    if follower.active_subscribers.where(:followed_id => followed.id).blank?
      new_followed = follower.active_subscribers.build(:followed_id => followed.id)
      if new_followed.save
        status = true
      end
    else
      status = false
      message = "#{target} already Subscribe!"
    end
    return status, message
  end

  def self.unsubsribe(requestor, target)
    status, message = false, ""
    followed = User.find_by_email(target)
    follower =  User.find_by_email(requestor)
    check_following = follower.active_subscribers.where(:followed_id => followed.id)
    if check_following.blank?
      status = false
      message = "#{target} unregister as subscribe!"
    else
      check_following.destroy_all
      status = "true"
    end
    return status , message
  end

  def self.push_message(params)
    members = []
    user = User.find_by_email(params[:sender])
    members << user.following.pluck(:email)
    members << user.friends.pluck(:email)
    if members.blank?
      status = false
      message = "#{params[:sender]} don't have friends & subscribe followed "
    else
      status = true
      members = members.flatten.uniq
    end
    return status, message , members
  end


end
