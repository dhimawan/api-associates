class Api::V1::UsersController < ApplicationController
  before_action :set_user, only: [:show, :update, :destroy]

  # GET /v1/users
  def index
    @users = User.all
    render json: @users
  end

  # GET /v1/users/1
  def show
    if @user.blank?
      render status: 404
    else
      render json: @user
    end
  end

  # POST /v1/users
  def create
    @user = User.new(user_params)

    if @user.save
      render json: @user, status: :created
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /v1/users/1
  def update
    if @user.update(user_params)
      render json: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
    
  end

  # DELETE /v1/users/1
  def destroy
    @user.destroy
  end

  # POST /v1/users/create_friend
  def create_friend
    status, message = User.create_friends(friend_params[:friends])
    render json: { :success => status , :message => message }
  end

  # GET /v1/users/friends
  def friends
    @user = User.find_by_email(params[:email])    
    if @user.blank?
      render status: 404 
    else
      list_mail = @user.friends.pluck(:email)
      render json: {:success => true , :friends => list_mail, :count => list_mail.count }
    end
  end

  # POST /v1/users/common_friends
  def common_friends
    if params[:friends].blank?
      render json: {:success => false, :errors => "params cannot be blank!"  },  status: :unprocessable_entity 
    else
      data = User.common_friends(params[:friends])
      render json: {:success => true , friends: data , count: data.count }
    end
  end
  
  # POST /v1/users/subscribe
  def subscribe
    status, message = User.add_subsribe(params["requestor"],params["target"])
    if status
      render json: {:success => status}, status: :created
    else
      render json: {:success => status, :message => message }, status: :unprocessable_entity
    end
  end

  # POST /v1/users/unsubscribe
  def unsubscribe
    status, message = User.unsubsribe(params["requestor"],params["target"])
    if status
      render json: {:success => status}
    else
      render json: {:success => status, :message => message }, status: :unprocessable_entity
    end
  end

  # POST /v1/users/push_message
  def push_message
    status, message, members = User.push_message(params)
    if status
      render json: {:success => status, :recipients => members }
    else
      render json: {:success => status, :message => message }, status: :unprocessable_entity
    end
  end

  private
    def set_user
      @user = User.find_by_id(params[:id])
      render status: 404 if @user.blank?
    end

    def user_params
      params.require(:user).permit(:firstname, :lastname, :email)
    end

    def friend_params
      params.permit(:friends => [])
    end
end
